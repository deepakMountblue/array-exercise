function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    const flat=[]
    for(let i=0;i<elements.length;i++){
        if(Array.isArray(elements[i])){
            const x=flatten(elements[i])
            for(let j=0;j<x.length;j++){
                flat.push(x[j])
            }
        }
        else{
            flat.push(elements[i])
        }
    }
    return flat
  }
  module.exports=flatten